package top.dj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 由于我们没有使用 spring.datasource.url、spring.datasource.username 默认的配置，
 * 而是自定义的 spring.datasource.master.jdbc-url、spring.datasource.master.username 等配置，
 * 所以我们需要排除 Spring 的自动配置类 DataSourceAutoConfiguration，防止在我们启动项目的时候，
 * 由于找不到 spring.datasource.url、spring.datasource.username 等配置而报了 “url” 未配置的错误。
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@MapperScan("top.dj.mapper")
@EnableScheduling // 开启定时任务
public class ReadWriteSplittingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReadWriteSplittingApplication.class, args);
    }

}
