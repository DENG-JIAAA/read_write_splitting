package top.dj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import top.dj.entity.Value;
import top.dj.service.ValueService;

import javax.validation.Valid;

/**
 * @Author: DengJia
 * @Date: 2021/9/26 21:55
 * @Description:
 */

@RestController
@RequestMapping("/val")
public class ValueController {
    @Autowired
    private ValueService valueService;

    @GetMapping("/getById/{id}")
    public Value getVal(@PathVariable("id") Integer id) {
        return valueService.selectByPrimaryKey(id);
    }

    @GetMapping("/getByVal/{val}")
    public Value getVal(@PathVariable("val") String val) {
        return valueService.selectByValue(val);
    }

    @GetMapping("/get0")
    public Value get0(@RequestParam(value = "id") Integer id) {
        return valueService.selectByPKDefault(id);
    }

    @GetMapping("/get1")
    public Value get1(Value value) {
        return valueService.selectFromMasterAndSlave(value.getId(), value.getValue());
    }

    @GetMapping("/get2")
    public Value get2(Value value) {
        return valueService.selectFromMasterAndSlaveWithDataSourceRoutingInMapper(value.getId(), value.getValue());
    }

    @GetMapping("/get3")
    public Value get3(Value value) {
        return valueService.selectFromMasterAndSlaveWithoutDataSourceRoutingInMapper(value.getId(), value.getValue());
    }

    @PostMapping("/add")
    // @DataSourceRouting(ClientDataSource.SLAVE)
    public Integer add(@RequestBody @Valid Value value) {
        return valueService.insertValue(value);
    }
}
