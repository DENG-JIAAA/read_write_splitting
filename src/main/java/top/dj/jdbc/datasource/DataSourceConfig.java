package top.dj.jdbc.datasource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import top.dj.jdbc.routing.ClientDataSource;
import top.dj.jdbc.routing.component.ClientDataSourceRouter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: DengJia
 * @Date: 2021/9/26 22:05
 * @Description:
 */

@Configuration
public class DataSourceConfig {

    @Bean(name = "masterDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.master")
    public DataSource masterDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "slaveDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.slave")
    public DataSource slaveDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "dynamicDataSource")
    public ClientDataSourceRouter dynamicDataSource(@Qualifier("masterDataSource") DataSource masterDataSource,
                                                    @Qualifier("slaveDataSource") DataSource slaveDataSource) {

        ClientDataSourceRouter dataSourceRouter = new ClientDataSourceRouter();
        dataSourceRouter.setDefaultTargetDataSource(masterDataSource);

        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(ClientDataSource.MASTER, masterDataSource);
        targetDataSources.put(ClientDataSource.SLAVE, slaveDataSource);
        dataSourceRouter.setTargetDataSources(targetDataSources);

        return dataSourceRouter;
    }
}
