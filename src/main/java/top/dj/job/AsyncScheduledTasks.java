package top.dj.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @Author: DengJia
 * @Date: 2021/10/22 10:18
 * @Description: 定时任务异步并行执行
 */

@Component
@EnableAsync
public class AsyncScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(AsyncScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /**
     * 固定延迟执行，据上一次调用成功后4秒才执行（5秒1输出）
     */
//    @Scheduled(fixedDelay = 4000)
//    @Async // 添加此注解后，会每4秒执行一次（4秒1输出）
    public void asyncScheduledTaskWithFixedDelay() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.info("固定延迟执行，当前时间：{}，当前线程：{}。", dateFormat.format(new Date()), Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
