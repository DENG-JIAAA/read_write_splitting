package top.dj.jdbc.routing;

import java.util.Objects;

/**
 * Context Holder that will hold the value for datasource routing for each different thread
 * (request).
 *
 * @Author: DengJia
 * @Date: 2021/9/26 22:37
 * @Description: 创建 ClientDataSourceContextHolder 来保存 ClientDataSource
 */

public class ClientDataSourceContextHolder {
    private static final ThreadLocal<ClientDataSource> CONTEXT = new ThreadLocal<>();

    public static void set(ClientDataSource clientDataSource) {
        CONTEXT.set(Objects.requireNonNull(clientDataSource, "clientDatabase cannot be null"));
    }

    public static ClientDataSource getClientDatabase() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }

}
