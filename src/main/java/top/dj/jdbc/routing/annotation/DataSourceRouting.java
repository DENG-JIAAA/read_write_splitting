package top.dj.jdbc.routing.annotation;

import top.dj.jdbc.routing.ClientDataSource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a method uses a specific datasource defined in {@link ClientDataSource}.
 *
 * @Author: DengJia
 * @Date: 2021/9/26 23:13
 * @Description: 通过注解 DataSourceRouting 来标识走 master/slave
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSourceRouting {

    ClientDataSource value() default ClientDataSource.MASTER;
}
