package top.dj.mapper;

import org.springframework.stereotype.Repository;
import top.dj.entity.Value;
import top.dj.jdbc.routing.ClientDataSource;
import top.dj.jdbc.routing.annotation.DataSourceRouting;

@Repository
public interface ValueMapper {

    @DataSourceRouting(value = ClientDataSource.MASTER)
    Value selectByPrimaryKey(Integer id);

    @DataSourceRouting(value = ClientDataSource.SLAVE)
    Value selectByValue(String val);

    Value selectByPKDefault(Integer id);

    Value selectByPrimaryKeyWithoutDataSourceRouting(Integer id);

    Value selectByValueWithoutDataSourceRouting(String val);

    int insertValue(Value value);
}
