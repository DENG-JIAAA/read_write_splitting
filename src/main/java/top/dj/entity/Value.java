package top.dj.entity;

import javax.validation.constraints.NotBlank;

/**
 * @Author: DengJia
 * @Date: 2021/9/26 21:57
 * @Description:
 */

public class Value {
    private Integer id;
    @NotBlank
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Value{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
