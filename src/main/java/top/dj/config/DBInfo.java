package top.dj.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: DengJia
 * @Date: 2021/10/22 12:19
 * @Description:
 */

@Component
@ConfigurationProperties(prefix = "dbbk")
public class DBInfo {
    private String host;
    private Integer port;
    private String username;
    private String password;
    private String dataBaseName;
    private String dataBackupPath;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }

    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public String getDataBackupPath() {
        return dataBackupPath;
    }

    public void setDataBackupPath(String dataBackupPath) {
        this.dataBackupPath = dataBackupPath;
    }
}
