package top.dj.jdbc.routing;

/**
 * 创建 ClientDataSource 枚举 定义主从库
 */
public enum ClientDataSource {
    MASTER, SLAVE
}
