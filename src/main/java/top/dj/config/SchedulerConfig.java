package top.dj.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * @Author: DengJia
 * @Date: 2021/10/22 10:08
 * @Description: 自定义线程池的执行
 */

@Configuration
public class SchedulerConfig implements SchedulingConfigurer {
    private static final int POOL_SIZE = 10;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        ThreadPoolTaskScheduler poolTaskScheduler = new ThreadPoolTaskScheduler();
        poolTaskScheduler.setPoolSize(POOL_SIZE);
        poolTaskScheduler.setThreadNamePrefix("MY-THREAD-TASK-SCHEDULER-");
        poolTaskScheduler.initialize();
        scheduledTaskRegistrar.setTaskScheduler(poolTaskScheduler);
    }
}
