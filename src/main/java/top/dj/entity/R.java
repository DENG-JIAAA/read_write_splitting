package top.dj.entity;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: DengJia
 * @Date: 2021/9/27 15:30
 * @Description: 通用返回
 */

public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", 0);
        put("msg", "success");
    }

    public static R error() {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "未知异常，请联系管理员！");
    }

    public static R error(String msg) {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg);
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R errorData(Object obj) {
        R r = new R();
        r.put("errorData", obj);
        return r;
    }

    public static R ok() {
        return new R();
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R data(Object obj) {
        R r = new R();
        r.put("data", obj);
        return r;
    }

    public static R data(String msg, Object obj) {
        R r = new R();
        r.put("msg", msg);
        r.put("data", obj);
        return r;
    }

    @Override
    public Object put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public Object getCode() {
        return super.get("code");
    }
}
