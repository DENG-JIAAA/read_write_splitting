package top.dj.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @Author: DengJia
 * @Date: 2021/10/21 17:20
 * @Description:
 */

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /**
     * 固定速率 每3秒执行一次
     */
//    @Scheduled(fixedRate = 3000)
    public void reportCurrentTimeWithFixedRate() {
        logger.info("固定速率执行，当前时间：{}，当前线程：{}。", dateFormat.format(new Date()), Thread.currentThread().getName());
    }

    /**
     * 固定延迟执行 据上一次调用成功后4秒才执行
     * 先执行一次方法（先睡1秒，再进行打印），方法执行完成后延迟4秒，再次执行方法。
     */
//    @Scheduled(fixedDelay = 4000)
    public void reportCurrentTimeWithFixedDelay() {
        try {
            TimeUnit.SECONDS.sleep(1);
            logger.info("固定延迟执行，当前时间：{}，当前线程：{}。", dateFormat.format(new Date()), Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始延迟，任务第一次执行将延迟5秒，之后以1秒的固定间隔执行
     */
//    @Scheduled(initialDelay = 5 * 1000, fixedRate = 1000)
    public void reportCurrentTimeWithInitialDelay() {
        logger.info("初始延迟执行，当前时间：{}，当前线程：{}。", dateFormat.format(new Date()), Thread.currentThread().getName());
    }

    /**
     * 使用cron表达式
     * 每分钟的1、2执行
     */
//    @Scheduled(cron = "1-2 * * * * ? ")
    public void reportCurrentTimeWithCronExpression() {
        // https://cron.qqe2.com/
        logger.info("使用cron表达式，当前时间：{}，当前线程：{}。", dateFormat.format(new Date()), Thread.currentThread().getName());
    }
}
