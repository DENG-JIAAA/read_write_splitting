package top.dj.service;

import top.dj.entity.Value;

public interface ValueService {

    /**
     * 把 @DataSourceRouting 放进 dao 层
     *
     * @param id 主键
     * @return value from master
     */
    Value selectByPrimaryKey(Integer id);

    /**
     * 把 @DataSourceRouting 放进 dao 层
     *
     * @param val 值
     * @return value from slave
     */
    Value selectByValue(String val);

    /**
     * 不加 @DataSourceRouting 注解 调用哪个库
     *
     * @param id    主键
     * @return master? slave?
     */
    Value selectByPKDefault(Integer id);

    /**
     * 把 @DataSourceRouting 放进 dao 层
     * 观察进入 Aspect 几次 (2次)
     *
     * @param id  主键
     * @param val 值
     * @return value from master and slave
     */
    Value selectFromMasterAndSlave(Integer id, String val);

    /**
     * 把 @DataSourceRouting 放进 service层 和 dao层，判断具体以哪个 datasource 为准。
     * (service层)
     */
    Value selectFromMasterAndSlaveWithDataSourceRoutingInMapper(Integer id, String value);

    Value selectFromMasterAndSlaveWithoutDataSourceRoutingInMapper(Integer id, String value);

    Integer insertValue(Value value);
}
